## 项目启动

## 思考过程

### 1.work1

首先，考虑到需要实现一个二叉树，我们需要定义一个节点类来表示树的节点，并在树类中保存根节点的引用。

插入操作需要按照二叉搜索树的性质逐级向下查找合适的位置，并插入新节点。

查询操作需要在树中进行比较，根据比较结果向左或向右移动，直到找到目标值或遍历完整个树。

删除操作是一个相对复杂的过程，需要考虑不同情况下的处理方式。对于删除节点，需要考虑其是否为叶子节点、是否有一个子节点或两个子节点，并根据不同情况进行相应的删除操作。

### 2.work2

当解决这个问题时，我们可以考虑使用哈希表来提高查找效率。

1. 我们需要在给定的整数集合中找到两个数，使它们的和等于目标值。最直观的方法是使用两层循环遍历所有可能的组合，但这样的时间复杂度是O(n^2)，效率较低。
2. 为了提高效率，我们可以使用哈希表来减少查找时间。我们可以采取以下步骤：

- 创建一个哈希表（例如，使用HashMap）来存储每个数及其索引。
- 遍历整数集合，并对于每个数，计算与目标值的差值（complement = target - nums[i]）。
- 检查哈希表中是否存在这个差值。如果存在，说明我们找到了两个数的和等于目标值。
- 如果存在，返回这两个数的索引；如果不存在，将当前数及其索引存入哈希表中。

3. 通过使用哈希表，我们可以在O(n)的时间复杂度内找到满足条件的两个数。这是因为哈希表提供了O(1)的查找时间。

### 3.work3

​	首先，我们需要生成包含这些网页URL的列表，根据给定的URL格式进行生成。

​	对于并发爬取，我们可以使用线程池来管理并发执行的任务。线程池可以同时处理多个任务，提高效率。我们使用ThreadPoolExecutor作为线程池，设置线程数为网页数量。然后，我们将每个URL对应的爬取任务（Callable）添加到任务列表中。爬取任务使用OkHttpClient发送HTTP请求，获取网页内容，并使用正则表达式提取数据字段的值。并发执行任务后，将每个任务的结果添加到结果列表中。

​	对于串行爬取，我们直接遍历网页URL列表，依次爬取每个网页。我们同样使用OkHttpClient发送HTTP请求，提取数据字段的值，并将结果添加到结果列表中。

​	最后，我们计算结果列表中所有值的总和，得到最终的结果。

### 4.work4

​	此题采用的是暴力穷举法，若想用更优的解法，可以考虑动态规划算法，但是动态规划不太熟悉，因此，用的是暴力穷举法，而且题目中只要求了是3-8个点，数据量比较小，暴力解法也不会过于影响性能。

1. 首先，我们需要考虑所有可能的连接顺序，也就是所有可能的点的排列。因为环路的边长和连接顺序相关，不同的连接顺序会导致不同的环路长度。
2. 为了生成所有可能的排列，我们使用了递归和回溯的方法。通过生成点集的全排列，我们可以考虑每个点在排列中的位置，从而考虑不同的连接顺序。
3. 对于每个排列，我们计算环路的边长之和。通过遍历排列中的每个点，并计算当前点到下一个点的距离，我们可以得到环路的边长。最后，将所有边长相加得到环路的总长度。
4. 在计算过程中，我们记录最小的环路边长之和，以便找到最短环路。



​	