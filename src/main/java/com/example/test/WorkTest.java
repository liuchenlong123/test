package com.example.test;

import com.example.test.work1.Tree;
import com.example.test.work2.FindNumSum;
import com.example.test.work3.GetWebData;
import com.example.test.work4.Point;
import com.example.test.work4.ShortestCycleAlgorithm;
import java.util.ArrayList;
import java.util.List;

public class WorkTest {
    public static void main(String[] args) {
        workTest1();
        workTest2();
        workTest3();
        workTest4();
    }

    private static void workTest1() {
        System.out.println("****************************************");
        System.out.println("题目一：自定义类型实现一个二叉树，API包含插入、查询、删除操作的定义");
        Tree tree = new Tree();
        // 插入操作
        tree.insert(50);
        tree.insert(30);
        tree.insert(20);
        tree.insert(40);
        tree.insert(70);
        tree.insert(60);
        tree.insert(80);

        // 查询操作
        System.out.println(tree.search(40)); // 输出: true
        System.out.println(tree.search(90)); // 输出: false

        // 删除操作
        tree.delete(20);
        tree.delete(70);

        System.out.println(tree.search(20)); // 输出: false
        System.out.println(tree.search(70)); // 输出: false
        System.out.println("题目一作答完成");
        System.out.println("****************************************");
        System.out.println();
    }

    private static void workTest2() {
        System.out.println("****************************************");
        System.out.println("题目二：给定一个整数集合与一个目标值，找出集合中和为目标值的两个数，定义并实现该方法");
        FindNumSum findNumSum = new FindNumSum();
        int[] nums = {2, 7, 11, 15};
        int target = 9;

        int[] result = findNumSum.findTwoSum(nums, target);
        if (result.length == 2) {
            System.out.println("索引: " + result[0] + ", " + result[1]);
            System.out.println("数值: " + nums[result[0]] + ", " + nums[result[1]]);
        } else {
            System.out.println("没有找到两个数的和等于目标值的情况");
        }
        System.out.println("题目二作答完成");
        System.out.println("****************************************");
        System.out.println();
    }

    private static void workTest3() {
        System.out.println("****************************************");
        System.out.println("题目三：从多个网页爬取内容，然后进行解析");
        GetWebData.countSum();
        System.out.println("题目三作答完成");
        System.out.println("****************************************");
        System.out.println();
    }

    private static void workTest4(){
        System.out.println("****************************************");
        System.out.println("题目四：最短环路算法");
        List<Point> points = new ArrayList<>();
        points.add(new Point(0, 1));
        points.add(new Point(1, 0));
        points.add(new Point(1, 1));
        points.add(new Point(0, 0));
        // 计算最短环路的边长之和
        double shortestCycle = ShortestCycleAlgorithm.calculateShortestCycle(points);
        System.out.println("最短环路的边长之和：" + shortestCycle);
        System.out.println("题目四作答完成");
        System.out.println("****************************************");
        System.out.println();

    }
}
