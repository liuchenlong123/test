package com.example.test.work4;
import java.util.ArrayList;
import java.util.List;
/**
 * 最短环路算法
 */


public class ShortestCycleAlgorithm {
    public static double calculateShortestCycle(List<Point> points) {
        // 生成所有点的全排列
        List<List<Integer>> permutations = generatePermutations(points.size());
        double shortestDistance = Double.MAX_VALUE;

        // 对每个排列计算环路的边长之和，并找到最小值
        for (List<Integer> permutation : permutations) {
            double distance = calculateCycleDistance(points, permutation);
            shortestDistance = Math.min(shortestDistance, distance);
        }

        return shortestDistance;
    }

    private static List<List<Integer>> generatePermutations(int n) {
        List<List<Integer>> permutations = new ArrayList<>();
        List<Integer> permutation = new ArrayList<>();
        boolean[] used = new boolean[n];

        // 递归生成全排列
        generatePermutationsUtil(n, permutation, used, permutations);

        return permutations;
    }

    private static void generatePermutationsUtil(int n, List<Integer> permutation, boolean[] used,
                                                 List<List<Integer>> permutations) {
        // 当排列长度达到 n 时，添加排列到结果列表
        if (permutation.size() == n) {
            permutations.add(new ArrayList<>(permutation));
            return;
        }

        for (int i = 0; i < n; i++) {
            if (!used[i]) {
                used[i] = true;
                permutation.add(i);
                generatePermutationsUtil(n, permutation, used, permutations);
                permutation.remove(permutation.size() - 1);
                used[i] = false;
            }
        }
    }

    private static double calculateCycleDistance(List<Point> points, List<Integer> permutation) {
        double distance = 0;
        int n = points.size();

        // 计算环路的边长之和
        for (int i = 0; i < n; i++) {
            int current = permutation.get(i);
            int next = permutation.get((i + 1) % n);
            distance += calculateDistance(points.get(current), points.get(next));
        }

        return distance;
    }

    private static double calculateDistance(Point p1, Point p2) {
        double dx = p1.x - p2.x;
        double dy = p1.y - p2.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

}
