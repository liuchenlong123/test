package com.example.test.work1;

/**
 * 树的数据结构
 * @author lcl
 */
public class TreeNode {
    int value;
    TreeNode left;
    TreeNode right;

    public TreeNode(int value) {
        this.value = value;
        left = null;
        right = null;
    }
}
