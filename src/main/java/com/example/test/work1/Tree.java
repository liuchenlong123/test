package com.example.test.work1;

/**
 * 自定义类型实现一个二叉树，API包含插入、查询、删除操作的定义
 * @author lcl
 */
public class Tree {
    private TreeNode root; // 二叉树的根节点

    /**
     * 向二叉树中插入一个新的值。
     */
    public void insert(int value) {
        if (root == null) {
            // 如果树为空，创建一个新节点并将其设置为根节点。
            root = new TreeNode(value);
        } else {
            TreeNode current = root;
            while (true) {
                if (value < current.value) {
                    // 如果要插入的值小于当前节点的值，则向左子节点移动。
                    if (current.left == null) {
                        // 如果左子节点为空，则在此位置插入新节点。
                        current.left = new TreeNode(value);
                        break;
                    }
                    current = current.left;
                } else {
                    // 如果要插入的值大于或等于当前节点的值，则向右子节点移动。
                    if (current.right == null) {
                        // 如果右子节点为空，则在此位置插入新节点。
                        current.right = new TreeNode(value);
                        break;
                    }
                    current = current.right;
                }
            }
        }
    }

    /**
     * 在二叉树中搜索指定的值。
     */
    public boolean search(int value) {
        TreeNode current = root;
        while (current != null) {
            if (value == current.value) {
                // 如果找到指定的值，返回true。
                return true;
            } else if (value < current.value) {
                // 如果要搜索的值小于当前节点的值，则向左子节点移动。
                current = current.left;
            } else {
                // 如果要搜索的值大于当前节点的值，则向右子节点移动。
                current = current.right;
            }
        }
        // 在遍历完整个树后仍未找到指定的值，返回false。
        return false;
    }

    /**
     * 从二叉树中删除指定的值。
     */
    public void delete(int value) {
        root = deleteNode(root, value);
    }

    /**
     * 递归地删除二叉树中的指定值。
     */
    private TreeNode deleteNode(TreeNode current, int value) {
        if (current == null) {
            // 如果当前节点为空，返回null。
            return null;
        }

        if (value == current.value) {
            // 如果要删除的值等于当前节点的值
            if (current.left == null) {
                // 如果左子节点为空，返回右子节点。
                return current.right;
            } else if (current.right == null) {
                // 如果右子节点为空，返回左子节点。
                return current.left;
            } else {
                // 如果左右子节点都不为空
                // 找到右子树中的最小值，并将其赋值给当前节点
                int smallestValue = findSmallestValue(current.right);
                current.value = smallestValue;
                // 在右子树中递归地删除最小值节点
                current.right = deleteNode(current.right, smallestValue);
                return current;
            }
        }

        if (value < current.value) {
            // 如果要删除的值小于当前节点的值，则在左子树中递归删除。
            current.left = deleteNode(current.left, value);
        } else {
            // 如果要删除的值大于当前节点的值，则在右子树中递归删除。
            current.right = deleteNode(current.right, value);
        }

        return current;
    }

    /**
     * 在二叉树中查找最小值。
     */
    private int findSmallestValue(TreeNode root) {
        if (root.left == null) {
            // 如果左子节点为空，说明当前节点为最小值节点。
            return root.value;
        } else {
            // 否则，在左子树中继续查找最小值。
            return findSmallestValue(root.left);
        }
    }
}
