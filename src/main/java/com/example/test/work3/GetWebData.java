package com.example.test.work3;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 从多个网页爬取内容，然后进行解析
 * @author lcl
 */
public class GetWebData {

    public static void countSum() {
        List<String> urls = generateUrls(10);

        // 并发爬取
        List<Integer> concurrentResults = concurrentCrawl(urls);
        int concurrentSum = calculateSum(concurrentResults);
        System.out.println("并行获取总数量: " + concurrentSum);

        // 串行爬取
        List<Integer> serialResults = serialCrawl(urls);
        int serialSum = calculateSum(serialResults);
        System.out.println("串行获取总数量: " + serialSum);
    }

    // 生成URL列表
    private static List<String> generateUrls(int count) {
        List<String> urls = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            String url = "http://dy-public.oss-cn-shenzhen.aliyuncs.com/interviewTestData/" + i + ".txt";
            urls.add(url);
        }
        return urls;
    }

    // 并发爬取
    private static List<Integer> concurrentCrawl(List<String> urls) {
        List<Integer> results = new ArrayList<>();
        ExecutorService executor = new ThreadPoolExecutor(urls.size(), urls.size(), 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
        List<Callable<Integer>> tasks = new ArrayList<>();

        for (String url : urls) {
            // 创建 Callable 任务，用于爬取数据
            Callable<Integer> task = () -> crawlData(url);
            tasks.add(task);
        }

        try {
            // 并发执行任务
            List<Future<Integer>> futures = executor.invokeAll(tasks);

            for (Future<Integer> future : futures) {
                try {
                    // 获取任务的结果并添加到结果列表中
                    int result = future.get();
                    results.add(result);
                } catch (Exception e) {
                    System.out.println("爬取URL时出错： " + e.getMessage());
                }
            }
        } catch (InterruptedException e) {
            System.out.println("执行任务时出错:" + e.getMessage());
        } finally {
            // 关闭线程池
            executor.shutdown();
        }

        return results;
    }

    // 串行爬取
    private static List<Integer> serialCrawl(List<String> urls) {
        List<Integer> results = new ArrayList<>();

        for (String url : urls) {
            try {
                // 串行爬取数据
                int result = crawlData(url);
                results.add(result);
            } catch (Exception e) {
                System.out.println("爬取URL出错: " + e.getMessage());
            }
        }

        return results;
    }

    // 爬取网页内容并提取data字段的值
    private static int crawlData(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        try (Response response = client.newCall(request).execute()) {
            String content = response.body().string();
            return extractDataValue(content);
        }
    }

    // 提取data字段的值
    private static int extractDataValue(String content) {
        String regex = "data\\s*:\\s*(\\d+)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        if (matcher.find()) {
            String dataValue = matcher.group(1);
            return Integer.parseInt(dataValue);
        }
        return 0;
    }

    // 计算结果列表的和
    private static int calculateSum(List<Integer> results) {
        int sum = 0;
        for (int result : results) {
            sum += result;
        }
        return sum;
    }
}