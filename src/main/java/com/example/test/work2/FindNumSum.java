package com.example.test.work2;

import java.util.HashMap;
import java.util.Map;

/**
 * 给定一个整数集合与一个目标值，找出集合中和为目标值的两个数，定义并实现该方法
 * @author lcl
 */
public class FindNumSum {
    public int[] findTwoSum(int[] nums, int target) {
        // 使用哈希表存储每个数及其索引
        Map<Integer, Integer> numMap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];
            if (numMap.containsKey(complement)) {
                // 找到了两个数的和等于目标值
                return new int[]{numMap.get(complement), i};
            }
            // 将数及其索引存入哈希表
            numMap.put(nums[i], i);
        }
        // 未找到满足条件的两个数
        return new int[0];
    }
}
